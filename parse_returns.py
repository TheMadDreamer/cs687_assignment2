import numpy as np
import sys
import matplotlib.pyplot as plt

filename = "./prob1/gridworld_cem_200_12_3eps_5trail_e2.txt"
filename = "gridworld_fchc_200eps_5trail_exp2.5.txt"
num_trails = 5
if __name__=="__main__":
    with open(filename, 'r') as f:
        arr = []
        for line in f:
            arr.append(line.rstrip('\n').strip('[').strip(']').split(']['))
        arr = np.array([float(x) for x in arr[0][0].split(',')])
        arr_n = np.reshape(np.array(arr), (num_trails, int(len(arr)/num_trails)))
        print(arr_n.shape)
        err = np.std(arr_n)
        avg = np.mean(arr_n, 0)
        plt.plot(np.arange(len(arr_n[1])), avg, color=(0,0.5,0.5))
        plt.grid(True)
        #plt.yticks(np.arange(-60, 20, 5))
        y_max = np.max(avg)
        plt.annotate("%.2f"% y_max, (0, y_max), (-400, y_max)) 
        #plt.plot(np.arange(len(arr_n[1])), y_max, 'b')
        # print(np.arange(len(arr_n[1])).shape)
        plt.axhline(y=y_max, color='r', linestyle='-', linewidth=0.5)
        plt.errorbar(range(len(arr_n[1])), avg, yerr=err, color='green', ecolor='orange', mfc='red', mec='green', alpha=0.5, fmt='none') #color='black', ecolor='lightgray', elinewidth=3, capsize=0)
        plt.show()
        #plt.fill_between(range(len(arr_n[1])), avg - err/2, avg + err/2, color='gray', alpha=0.2)
        #arr = [float(x) for x in arr[0]]
        #print(len(arr))
