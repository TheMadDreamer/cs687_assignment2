import numpy as np
from IPython import embed
import matplotlib

matplotlib.rcParams['pdf.fonttype'] = 42  # avoid type 3 fonts
matplotlib.rcParams['ps.fonttype'] = 42
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

from rl687.policies.tabular_softmax import TabularSoftmax
from rl687.environments.gridworld import Gridworld
from rl687.environments.cartpole import Cartpole
from rl687.policies.ContinuousPolicy import ContinuousPolicy
from rl687.agents.cem import CEM
from rl687.agents.fchc import FCHC
from rl687.agents.ga import GA

np.random.seed(69)
global_returns = []
attempt = 0

reward_log_file = "gridworld_fchc_6000eps_5trail_exp1.5_2.txt"
fig_name = "gridworld_fchc_6000eps_5trail_exp1.5_2.png"
policy_vis_file = "policy_vis_fchc_gridworld_6000eps_5trail_exp1.5_2.txt"
max_nepisodes = 6000

# reward_log_file = "gridworld_ga_200_16_5eps_5trail_alpha2_Ke5_Kp8.txt"
# fig_name = "gridworld_ga_200_16_5eps_5trail_alpha2_Ke5_Kp8.png"
# policy_vis_file = "policy_vis_ga_gridworld_ga_200_16_5eps_5trail_alpha2_Ke5_Kp8.txt"
# max_nepisodes = 16000

num_trails = 50
global_avg_returns = np.zeros(max_nepisodes) # Over trails

def vis_policy_gridworld(policy, fout, attempt_id):    
    fout.write("===============" + str(attempt_id) + "==============\n")
    for r in range(5):
        fout.write("----------------------------------\n")
        for c in range(5):
            actions = policy.getActionProbabilities(r*5 + c)
            action = np.argmax(actions)
            if action == 0:
                pa = "^"
            elif action == 1:
                pa = "v"
            elif action == 2:
                pa = "<"
            elif action == 3:
                pa = ">"
            fout.write("| " + pa + " ")
        fout.write("|\n")
    fout.write("--------------------------------------\n")
    # fout.write("================================\n")

def initPopulationFunction_gridworld(pop_size, len_params=25*4):
    # np.random.seed(482)
    theta_0 = np.zeros(len_params)
    pops = np.array([np.random.multivariate_normal(theta_0, 1.0*np.identity( len(theta_0) ) ) for x in range(pop_size)])
    # pops = np.zeros((pop_size, len_params))
    return pops

def evaluate_gridworld(policy_params, num_episodes, basis_k=4):
    policy = TabularSoftmax(25, 4)
    policy.parameters = policy_params
    
    if (num_episodes <= 0):
        return -1

    gworld = Gridworld()

    returns = []
    gamma = 0.9 # This gamma is different from gamma of gridworld environment
    fout = open(policy_vis_file, 'a')
    for episode in range(num_episodes):
        gworld.reset()
        ret = 0
        step = 0
        max_steps = 200
        # if (episode == 0):
        #     print(policy.parameters)
        while (True):
            action = policy.samplAction(gworld.state)
            s, r, e = gworld.step(action)
            ret += (gamma**step)*r
            step += 1
            if (gworld.isEnd):
                break
            if (step > max_steps):
                ret = -50
                break
        returns.append(ret)
        global_returns.append(ret)
        global attempt
        attempt += 1
        if (attempt % (num_episodes*20) == 0):
            print("Attempt :", attempt/num_episodes," Return :", ret)

        if (attempt % (num_episodes*40) == 0):
            vis_policy_gridworld(policy, fout, attempt)
    returns = np.array(returns)
    return (np.sum(returns)/num_episodes)
    
def problem1():
    """
    Apply the CEM algorithm to the More-Watery 687-Gridworld. Use a tabular 
    softmax policy. Search the space of hyperparameters for hyperparameters 
    that work well. Report how you searched the hyperparameters, 
    what hyperparameters you found worked best, and present a learning curve
    plot using these hyperparameters, as described in class. This plot may be 
    over any number of episodes, but should show convergence to a nearly 
    optimal policy. The plot should average over at least 500 trials and 
    should include standard error or standard deviation error bars. Say which 
    error bar variant you used. 
    """
    print("Solving Gridworld with CE Method...")

    num_states = 25
    num_actions = 4
    tabular_SF = TabularSoftmax(num_states, num_actions)

    # Initialize params
    sigma = 1.5
    theta_0 = np.array(num_states*[0., 0., 0., 0.])
    theta = np.random.multivariate_normal(theta_0, sigma*np.identity(len(theta_0)))
    population_K = 12
    elite_population_K = 3
    num_episodes_N = 5
    epsilon = 2

    global global_avg_returns
    global attempt
    global_avg_returns = np.zeros(max_nepisodes*population_K*num_episodes_N)
    agent = CEM(theta, sigma, population_K, elite_population_K, num_episodes_N, evaluate_gridworld, epsilon)

    for trail in range(num_trails):
        # np.random.seed(trail*2)
        while (attempt < max_nepisodes*population_K*num_episodes_N):
            agent.train()
        agent.reset()
        attempt = 0
        # print(len(global_returns[-max_nepisodes*population_K*num_episodes_N:]))
        global_avg_returns = global_avg_returns + np.array(global_returns[-max_nepisodes*population_K*num_episodes_N:])
    global_avg_returns = global_avg_returns/num_trails

def problem2():
    """
    Repeat the previous question, but using first-choice hill-climbing on the 
    More-Watery 687-Gridworld domain. Report the same quantities.
    """
    print("Solving Gridworld with FCHC Method...")
    num_states = 25
    num_actions = 4

    # Initialize params    
    theta_0 = np.array(num_states*[0., 0., 0., 0.])
    exploration_param = 1.5
    theta = np.random.multivariate_normal(theta_0, exploration_param*np.identity( len(theta_0)) ) # Initial mean paramter
    num_episodes_N = 5

    global global_avg_returns
    global attempt
    agent = FCHC(theta, exploration_param, evaluate_gridworld, num_episodes_N)
    for trail in range(num_trails):
        # np.random.seed(trail*2)
        while (attempt < max_nepisodes):
            rets = agent.train()
        agent.reset()
        print(len(global_returns[-max_nepisodes:]))
        global_avg_returns = global_avg_returns + np.array(global_returns[-max_nepisodes:])
        attempt = 0
    global_avg_returns = global_avg_returns/num_trails

def problem3():
    """
    Repeat the previous question, but using the GA (as described earlier in 
    this assignment) on the More-Watery 687-Gridworld domain. Report the same 
    quantities.
    """
    print("Solving Gridworld with GA")
    num_states = 25
    num_actions = 4
    
    pop_size = 16
    num_elite = 4
    num_episodes_N = 5
    K_parent_truncation_index = 8
    alpha = 2.0
    n_generations = 200
    
    # theta_0 = np.array(num_states*[0., 1., 0., 1.])
    agent = GA(pop_size, evaluate_gridworld, initPopulationFunction_gridworld, num_elite, num_episodes_N, \
                parentTruncationIndex=K_parent_truncation_index, alpha=alpha, n_generations=n_generations)
    global global_avg_returns
    global_avg_returns = np.zeros(num_episodes_N*n_generations*pop_size)
    global attempt
    for trail in range(num_trails):
        # np.random.seed(trail*2)
        agent.reset()
        for g in range(n_generations):
            agent.train()
        global_avg_returns = global_avg_returns + np.array(global_returns[-num_episodes_N*n_generations*pop_size:])
        attempt = 0
    global_avg_returns /= num_trails

#################### linear function approximation ####################
def lin_approx_state(state, basis_k=3):
    from itertools import product
    c = np.array( list( product( range(basis_k), repeat=len(state)) ) )
    # print(c)
    # NORMALIZE State
    state = (state)/np.array([3., 3., 3., 3.]) #  - np.array([-3, -3, -np.pi/12, -4])
    feature_args = np.matmul(c, state)*2*np.pi
    feature_vector = np.cos(feature_args)
    # if (attempt % 50 == 0):
    #     print(feature_args)
    return feature_vector

def evaluate_cartpole(policy_params, num_episodes, basis_k=4):
    
    if (num_episodes <= 0):
        return -1

    num_actions = 2

    cpole = Cartpole()
    feature_vector = lin_approx_state(cpole.state, basis_k)
    policy = ContinuousPolicy(num_actions, len(feature_vector))
    policy.parameters = policy_params

    returns = []
    gamma = 1 # This gamma is different from gamma of cartpole environment
    fout = open("policy_vis_cartpole_cem.txt", 'a')
    for episode in range(num_episodes):
        cpole.reset()
        ret = 0
        while (True):
            action = policy.samplAction( lin_approx_state(cpole.state, basis_k) )
            s, r, e = cpole.step( action )
            ret += r
            if (cpole.isEnd):
                break
        returns.append(ret)
        global_returns.append(ret)
        global attempt
        attempt += 1
        if (attempt % (num_episodes*10) == 0):
            print("Attempt :", attempt/(num_episodes)," Return :", ret)
        if (attempt % (num_episodes*40) == 0):
            print("state : ", cpole.state)

        # if (attempt % (num_episodes*10) == 0):
            # vis_policy_gridworld(policy, fout, attempt)  
    returns = np.array(returns)
    return (np.sum(returns)/num_episodes)

def initPopulationFunction_cartpole(pop_size, len_params=2*256):
    theta_0 = np.zeros(len_params)
    pops = np.array([np.random.multivariate_normal(theta_0, 1.0*np.identity( len(theta_0) ) ) for x in range(pop_size)])
    # pops = np.zeros((pop_size, len_params))
    return pops

#######################################################################

def problem4():
    """
    Repeat the previous question, but using the cross-entropy method on the 
    cart-pole domain. Notice that the state is not discrete, and so you cannot 
    directly apply a tabular softmax policy. It is up to you to create a 
    representation for the policy for this problem. Consider using the softmax 
    action selection using linear function approximation as described in the notes. 
    Report the same quantities, as well as how you parameterized the policy. 
    
    """
    print("Solving Cartpole with CE Method...")

    num_actions = 2
    selected_basis_k = 3
    initial_state = np.array([0., 0., 0., 0.])
    feature_len = len( lin_approx_state(initial_state, selected_basis_k) )
    contpolicy = ContinuousPolicy(num_actions, len(lin_approx_state(initial_state, basis_k=selected_basis_k) ) )

    # Initialize params
    sigma = 1.0
    theta_0 = np.zeros(num_actions*feature_len)
    theta = np.random.multivariate_normal(theta_0, sigma*np.identity(len(theta_0)) )
    population_K = 20
    elite_population_K = 5
    num_episodes_N = 5
    epsilon = 1.5

    global global_avg_returns
    global attempt
    agent = CEM(theta, sigma, population_K, elite_population_K, num_episodes_N, evaluate_cartpole, epsilon)
    for trail in range(num_trails):
        attempt = 0
        while (attempt < num_episodes_N*max_nepisodes*population_K):
            rets = agent.train()
        agent.reset()
        print(len(global_returns[-num_episodes_N*population_K*max_nepisodes:]))
        global_avg_returns = global_avg_returns + np.array(global_returns[-max_nepisodes*population_K*num_episodes_N:])
    global_avg_returns = global_avg_returns/num_trails

def problem5():
    """
    Repeat the previous question, but using first-choice hill-climbing (as 
    described in class) on the cart-pole domain. Report the same quantities 
    and how the policy was parameterized. 
    
    """
    print("Solving Cartpole with FCHC Method...")

    num_actions = 2
    selected_basis_k=4
    initial_state = np.array([0., 0., 0., 0.])
    feature_len = len(lin_approx_state(initial_state, selected_basis_k))
    contpolicy = ContinuousPolicy(num_actions, len(lin_approx_state(initial_state, selected_basis_k) ) )

    # Initialize params
    exploration_param = 1.9
    theta_0 = np.zeros( num_actions*feature_len )
    theta = np.random.multivariate_normal(theta_0, exploration_param*np.identity( len(theta_0) ) )
    population_K = 15
    num_episodes_N = 5

    global global_avg_returns
    global attempt
    agent = FCHC(theta, exploration_param, evaluate_cartpole, num_episodes_N, selected_basis_k)
    for trail in range(num_trails):
        while (attempt < max_nepisodes):
            agent.train()
        agent.reset()
        print( len(global_returns[-max_nepisodes:]) )
        global_avg_returns = global_avg_returns + np.array( global_returns[-max_nepisodes:] )
        attempt = 0
    global_avg_returns = global_avg_returns/num_trails

def problem6():
    """
    Repeat the previous question, but using the GA (as described earlier in 
    this homework) on the cart-pole domain. Report the same quantities and how
    the policy was parameterized. 
    """
    print("Solving Cartpole with GA ...")
    num_actions = 2
    num_episodes_N = 7
    selected_basis_k = 4
    initial_state = np.array([0., 0., 0., 0.])
    feature_len = len(lin_approx_state(initial_state, selected_basis_k))

    # Initialize params
    exploration_param = 0.9
    K_parent_truncation_index = 12
    alpha = 0.5
    n_generations = 300
    theta_0 = np.zeros( num_actions*feature_len )
    theta = np.random.multivariate_normal(theta_0, exploration_param*np.identity( len(theta_0) ) )
    population_K = 15
    elite_population_K = 5
    num_episodes_N = 5

    global global_avg_returns
    global attempt
    agent = GA(population_K, evaluate_cartpole, initPopulationFunction_cartpole, elite_population_K, num_episodes_N, \
                   parentTruncationIndex=K_parent_truncation_index, alpha=alpha, n_generations=n_generations)
    global_avg_returns = np.zeros(num_episodes_N*n_generations*population_K)    
    for trail in range(num_trails):
        agent.reset()
        for g in range(n_generations):
            agent.train()
        global_avg_returns = global_avg_returns + np.array(global_returns[-num_episodes_N*n_generations*population_K:])
        attempt = 0
    global_avg_returns = global_avg_returns/num_trails

def main():
        
    # max_nepisodes = 2000
    # attempt = 0
    global global_returns
    problem3()
    # print(len(global_returns))   
    fout = open(reward_log_file, 'w+')
    fout.write(str(global_returns))
    fout.close()

    # print(len(global_returns))
    global_returns = np.reshape(np.array(global_returns), (num_trails, int(len(global_returns)/num_trails) ))
    std_returns = np.std(global_returns)
    print(std_returns.shape)
    y_max = np.max(global_avg_returns)
    plt.plot(global_avg_returns, color='orange')
    plt.axhline(y=y_max, color='r', linestyle='-', linewidth=0.5)
    plt.annotate("%.2f" % y_max, (0, y_max))
    plt.errorbar(np.arange(len(global_avg_returns)), global_avg_returns, yerr=std_returns, ecolor='orange', \
                                    mfc='red', mec='green', alpha=0.35)
    plt.grid(True)
    # plt.fill_between(np.arange(len(global_avg_returns)), global_avg_returns - std_returns/2, \
    #     global_avg_returns + std_returns/2)
    plt.xlabel("Episodes")
    plt.ylabel("Return")
    # plt.ylim(-50, 10)
    plt.savefig(fig_name)
    plt.show()


if __name__ == "__main__":
    main()
