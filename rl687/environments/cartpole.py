import numpy as np
from typing import Tuple
from .skeleton import Environment


class Cartpole(Environment):
    """
    The cart-pole environment as described in the 687 course material. This
    domain is modeled as a pole balancing on a cart. The agent must learn to
    move the cart forwards and backwards to keep the pole from falling.

    Actions: left (0) and right (1)
    Reward: 1 always

    Environment Dynamics: See the work of Florian 2007
    (Correct equations for the dynamics of the cart-pole system) for the
    observation of the correct dynamics.
    """

    def __init__(self):
        self._name = "Cartpole"
        self._action = None
        self._reward = 1
        self._isEnd = False
        self._gamma = 1.0

        # define the state # NOTE: you must use these variable names
        self._x = 0.  # horizontal position of cart
        self._v = 0.  # horizontal velocity of the cart
        self._theta = 0.  # angle of the pole
        self._dtheta = 0.  # angular velocity of the pole
        # self.state = [self._x, self._v, self._theta, self._dtheta]

        # dynamics
        self._g = 9.8  # gravitational acceleration (m/s^2)
        self._mp = 0.1  # pole mass
        self._mc = 1.0  # cart mass
        self._l = 0.5  # (1/2) * pole length
        self._dt = 0.02  # timestep
        self._t = 0.0  # total time elapsed  NOTE: USE must use this variable

    @property
    def name(self) -> str:
        return self._name

    @property
    def reward(self) -> float:
        return self._reward

    @property
    def gamma(self) -> float:
        return self._gamma

    @property
    def action(self) -> int:
        return self._action

    @property
    def isEnd(self) -> bool:
        return self._isEnd

    @property
    def state(self) -> np.ndarray:
        return np.asarray([self._x, self._v, self._theta, self._dtheta])
        # return state

    def nextState(self, state: np.ndarray, action: int) -> np.ndarray:
        """
        Compute the next state of the pendulum using the euler approximation to the dynamics
        """
        x, x_dot, theta, theta_dot = state
        force = 10 if (action > 0) else -10
        w_dot = (self._g*np.sin(theta) + np.cos(theta)*((- force - self._mp*self._l*np.sin(theta)*(theta_dot**2) )/(self._mp + self._mc) ) )/ \
                        (self._l*(4/3 - (self._mp*(np.cos(theta)*np.cos(theta)))/(self._mc + self._mp)))
        v_dot = (force + self._mp*self._l*(theta_dot**2 * np.sin(theta) - w_dot*np.cos(theta)))/(self._mp + self._mc)
        state_dot = np.array([x_dot, v_dot, theta_dot, w_dot])
        x_next = state + self._dt*state_dot
        return x_next

    def R(self, state: np.ndarray, action: int, nextState: np.ndarray) -> float:
        # if (state[2] > np.pi/12 or state[2] < -np.pi/12):
        #     return -10
        # self.isEnd = self.terminal()
        # if (self.isEnd):
        #     return 0
        # else:
        self._reward = 1
        return 1

    def step(self, action: int) -> Tuple[np.ndarray, float, bool]:
        """
        takes one step in the environment and returns the next state, reward, and if it is in the terminal state
        """
        next_state = self.nextState(self.state, action)
        # print(self.state)
        reward = self.R(self.state, action, next_state)

        # Update private variables
        self._t += self._dt
            
        self._x = next_state[0]
        self._v = next_state[1]
        self._theta = next_state[2]
        self._dtheta = next_state[3]
        isTerminal = self.terminal()
        if (isTerminal == True):
            self._isEnd = True
        self._action = action # 10 if (action > 0) else -10
        

        return (next_state, reward, isTerminal)
        

    def reset(self) -> None:
        """
        resets the state of the environment to the initial configuration
        """
        self._x = 0
        self._v = 0
        self._theta = 0
        self._dtheta = 0
        self._t = 0
        self._isEnd = False
        self._action = None
        self._reward = 0

    def terminal(self) -> bool:
        """
        The episode is at an end if:
            time is greater that 20 seconds
            pole falls |theta| > (pi/12.0)
            cart hits the sides |x| >= 3
        """
        if (self._t > 20-1e-8):
            return True
        if (self._theta < -np.pi/12.0 or self._theta > np.pi/12.0):
            return True
        if (self._x <= -3 or self._x >= 3):
            return True
        else:
            return False

