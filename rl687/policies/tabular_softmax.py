import numpy as np
from .skeleton import Policy
from typing import Union

class TabularSoftmax(Policy):
    """
    A Tabular Softmax Policy (bs)


    Parameters
    ----------
    numStates (int): the number of states the tabular softmax policy has
    numActions (int): the number of actions the tabular softmax policy has
    """

    def __init__(self, numStates:int, numActions: int):
        
        #The internal policy parameters must be stored as a matrix of size
        #(numStates x numActions)
        self._theta = np.zeros((numStates, numActions))
        
        

    @property
    def parameters(self)->np.ndarray:
        """
        Return the policy parameters as a numpy vector (1D array).
        This should be a vector of length |S|x|A|
        """
        return self._theta.flatten()
    
    @parameters.setter
    def parameters(self, p:np.ndarray):
        """
        Update the policy parameters. Input is a 1D numpy array of size |S|x|A|.
        """
        self._theta = p.reshape(self._theta.shape)

    def __call__(self, state:int, action=None)->Union[float, np.ndarray]:
        if action is None:
            return self.getActionProbabilities(state)
        else:
            return self.getActionProbabilities(state)[action]
        

    def samplAction(self, state:int)->int:
        """
        Samples an action to take given the state provided. 
        
        output:
            action -- the sampled action
        """
        p_actions = self.getActionProbabilities(state)
        sampledaction = np.random.choice(len(p_actions), p=p_actions)

        # Alternative implementation of np random choice : precision .001
        # p_actions_rounded = np.floor(1000*p_actions)
        # randint = np.random.randint(1000)
        # if (randint < p_actions_rounded[0]):
        #     sampledaction = 0
        # elif (randint < p_actions_rounded[1]):
        #     sampledaction = 1
        # elif (randint < p_actions_rounded[2]):
        #     sampledaction = 2
        # else:
        #     sampledaction = 3

        return sampledaction

    def getActionProbabilities(self, state:int)->np.ndarray:
        """
        Compute the softmax action probabilities for the state provided. 
        
        output:
            distribution -- a 1D numpy array representing a probability 
                            distribution over the actions. The first element
                            should be the probability of taking action 0 in 
                            the state provided.
        """

        actions = self._theta[state]
        max_action = np.max(actions)
        actions = actions - max_action
        actions[np.where(actions > 300)] = 300
        actions[np.where(actions < -300)] = -300
        actions_sf = np.exp(actions)
        actions_sf = actions_sf/np.sum(actions_sf)
        return actions_sf
