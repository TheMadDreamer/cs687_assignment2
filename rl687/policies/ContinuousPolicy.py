import numpy as np
from .skeleton import Policy
from typing import Union

class ContinuousPolicy(Policy):
    """
    A Tabular Softmax Policy (bs)


    Parameters
    ----------
    numStates (int): the number of states the tabular softmax policy has
    numActions (int): the number of actions the tabular softmax policy has
    """

    def __init__(self, numActions:int, paramlen:int):
        
        #The internal policy parameters must be stored as a matrix of size
        #(numStates x numActions)
        # print(numActions, paramlen)
        self._theta = np.zeros((numActions, paramlen))
        
        

    @property
    def parameters(self)->np.ndarray:
        """
        Return the policy parameters as a numpy vector (1D array).
        This should be a vector of length |S|x|A|
        """
        return self._theta
    
    @parameters.setter
    def parameters(self, p:np.ndarray):
        """
        Update the policy parameters. Input is a 1D numpy array of size |S|x|A|.
        """
        self._theta = p.reshape(self._theta.shape)

    def __call__(self, state:int, action=None)->Union[float, np.ndarray]:
        if action is None:
            return self.samplAction(state)
        else:
            return self.getActionProbabilities(state)[action]
        

    def samplAction(self, state:np.ndarray)->int:
        """
        Samples an action to take given the state provided. 
        
        output:
            action -- the sampled action
        """
        p_actions = self.getActionProbabilities(state)
        sampledaction = np.random.choice(len(p_actions), p=p_actions)
        return sampledaction

    def getActionProbabilities(self, state:np.ndarray)->int:
        """
        Compute the softmax action probabilities for the state provided. 
        
        output:
            distribution -- a 1D numpy array representing a probability 
                            distribution over the actions. The first element
                            should be the probability of taking action 0 in 
                            the state provided.
        """

        actions = np.matmul(self._theta, state)
        max_action = np.argmax(actions)  
        actions = actions - max_action
        actions[np.where(actions > 300)] = 300
        actions[np.where(actions < -300)] = -300
        actions_sf = np.exp(actions)
        actions_sf = actions_sf/np.sum(actions_sf)      
        return actions_sf
