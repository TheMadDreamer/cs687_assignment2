import numpy as np
from .bbo_agent import BBOAgent

from typing import Callable


class GA(BBOAgent):
    """
    A canonical Genetic Algorithm (GA) for policy search is a black box 
    optimization (BBO) algorithm. 
    
    Parameters
    ----------
    populationSize (int): the number of individuals per generation
    numEpisodes (int): the number of episodes to sample per policy         
    evaluationFunction (function): evaluates a parameterized policy
        input: a parameterized policy theta, numEpisodes
        output: the estimated return of the policy            
    initPopulationFunction (function): creates the first generation of
                    individuals to be used for the GA
        input: populationSize (int)
        output: a numpy matrix of size (N x M) where N is the number of 
                individuals in the population and M is the number of 
                parameters (size of the parameter vector)
    numElite (int): the number of top individuals from the current generation
                    to be copied (unmodified) to the next generation
    
    """

    def __init__(self, populationSize:int, evaluationFunction:Callable, 
                 initPopulationFunction:Callable, numElite:int=1, numEpisodes:int=10, parentTruncationIndex:int=8, alpha:float=2.5, n_generations:int=30):
    
        self._name = "Genetic_Algorithm"
        self._populationSize = populationSize
        self._initPopulationFunction = initPopulationFunction
        self._evaluationFunction = evaluationFunction
        self._original_population = self._initPopulationFunction(self._populationSize)
        self._numElite = numElite
        self._numEpisodes = numEpisodes


        self._parentTruncationIndex = parentTruncationIndex
        self._alpha = alpha
        self._n_generations = n_generations
        # # self._attempts = 0
        self._population = self._original_population
        self._bestparams = self._population[0]
        self._bestreturn = -100


    @property
    def name(self)->str:
        return self._name
    
    @property
    def parameters(self)->np.ndarray:
        return self._bestparams

    def _mutate(self, parent:np.ndarray)->np.ndarray:
        """
        Perform a mutation operation to create a child for the next generation.
        The parent must remain unmodified. 
        
        output:
            child -- a mutated copy of the parent
        """
        params = parent + self._alpha*np.random.normal(np.zeros(len(parent)), 1)
        return params

    def _get_parents(self, Kp, params_arr):
        parents = params_arr[:Kp]
        return parents

    def _get_children(self, alpha, parents, num_children):
        parent_ids = np.random.randint(len(parents), size=num_children)
        children_params = []
        for pid in parent_ids:
            params = self._mutate(parents[pid])
            children_params.append(params)
        return np.array(np.reshape(children_params, (num_children, len(parents[0]))))

    def train(self)->np.ndarray:
        # fout_vis = open("ga_policy_vis_2.txt", 'w+')
        # # fout = open("ga_policy_track_2.txt", 'w+')
        # for g in range(self._n_generations):
        ret_arr = np.array([])
        for k in range(self._populationSize):
            params = self._population[k]
            J_k = self._evaluationFunction(params, self._numEpisodes)
            ret_arr = np.append(ret_arr, J_k)
        
        arg_sortparams = np.argsort(ret_arr)
        arg_sortrets_rev = arg_sortparams[::-1]
        population_sorted = self._population[arg_sortrets_rev]
        parents = self._get_parents(self._parentTruncationIndex, population_sorted)
        children = self._get_children(self._alpha, parents, self._populationSize - self._numElite)
        # print(children.shape)
        self._population = np.concatenate((population_sorted[:self._numElite], children))
        # np.append(self._ret_collection, np.mean(ret_arr))

        ############ DEBUG CODE ########## 
        # print("Generation ", g, " : ", ret_arr[arg_sortparams[-1]])
        # if ((g + 1) % 5 == 0):
        #     for s in range(num_states):
        #         fout.write(str(s) + " : " + str(ga_policy.getActionProbabilities(s)) + "\n")
        #     fout.write("=====================================")
        #     vis_policy_gridworld(ga_policy, fout_vis, g)

        # print(population_sorted.shape)
        if (self._bestreturn < ret_arr[arg_sortrets_rev[0]]):
            self._bestreturn = ret_arr[arg_sortrets_rev[0]]
            self._bestparams = population_sorted[0]
        # print("Gen ", g, " : ", ret_arr[arg_sortrets_rev][0])
        return population_sorted[0]


    def reset(self)->None:
        # self._population = self._initPopulationFunction(self._populationSize)
        self._population = self._original_population
        self._bestparams = self._population[0]
        self._bestreturn = -100

        # self._attempts = 0
        # self._ret_collection = np.array([])