import numpy as np
from .bbo_agent import BBOAgent

from typing import Callable

class CEM(BBOAgent):
    """
    The cross-entropy method (CEM) for policy search is a black box optimization (BBO)
    algorithm. This implementation is based on Stulp and Sigaud (2012). Intuitively,
    CEM starts with a multivariate Gaussian dsitribution over policy parameter vectors.
    This distribution has mean theta and covariance matrix Sigma. It then samples some
    fixed number, K, of policy parameter vectors from this distribution. It evaluates
    these K sampled policies by running each one for N episodes and averaging the
    resulting returns. It then picks the K_e best performing policy parameter
    vectors and fits a multivariate Gaussian to these parameter vectors. The mean and
    covariance matrix for this fit are stored in theta and Sigma and this process
    is repeated.

    Parameters
    ----------
    sigma (float): exploration parameter
    theta (numpy.ndarray): initial mean policy parameter vector
    popSize (int): the population size
    numElite (int): the number of elite policies
    numEpisodes (int): the number of episodes to sample per policy
    evaluationFunction (function): evaluates the provided parameterized policy.
        input: theta_p (numpy.ndarray, a parameterized policy), numEpisodes
        output: the estimated return of the policy
    epsilon (float): small numerical stability parameter
    """

    def __init__(self, theta:np.ndarray, sigma:float, popSize:int, numElite:int, numEpisodes:int, evaluationFunction:Callable, epsilon:float=0.0001, basis_k:int=4):
        # Initialize params
        self._name = "Cross_Entropy_Method"

        self._theta = theta #TODO: set this value to the current mean parameter vector
        self._sigma = sigma
        self._Sigma = self._sigma*np.identity(len(theta)) #TODO: set this value to the current covariance matrix
        self._popSize = popSize
        self._numElite = numElite
        self._numEpisodes = numEpisodes        
        self._epsilon = epsilon
        self._evaluationFunction = evaluationFunction

        self._attempts = 0
        # self._max_nepisodes = 30000
        # self._ret_collection = np.array([])
        self._orig_theta = theta
        self._orig_sigma = self._Sigma
        self._basis_k = basis_k
       

    @property
    def name(self)->str:
        return self._name
    
    @property
    def parameters(self)->np.ndarray:
        return self._theta

    def train(self)->np.ndarray:
        theta_arr = np.array([], dtype=float)
        ret_arr = np.array([], dtype=float)
        cov_arr = np.array([], dtype=float)
        
        thetas = np.random.multivariate_normal(self._theta, self._Sigma, self._popSize)
        thetas = np.reshape(thetas, (self._popSize, len(self._theta) ) )
        for k in range(0, self._popSize):
            theta_k = thetas[k] # np.random.multivariate_normal(self._theta, self._covariance)        
            J_k = self._evaluationFunction(theta_k, self._numEpisodes, self._basis_k)
            theta_arr = np.append(theta_arr, theta_k)
            ret_arr = np.append(ret_arr, [J_k])
            
        theta_arr_sort_arg = np.argsort(ret_arr)
        reversed_theta_arr_sort_arg = theta_arr_sort_arg[::-1]

        theta_arr = np.reshape(theta_arr, (self._popSize, len(self._theta) ) )
        theta_arr = theta_arr[reversed_theta_arr_sort_arg[:self._numElite]]
        self._theta = (1/self._numElite)*np.sum(theta_arr, 0)
        diff = theta_arr - self._theta
        cov = np.zeros( (len(self._theta), len(self._theta)) )
        for d in diff: 
            cov = cov + np.matmul(np.array([d]).T, np.array([d]))
        # print(np.matmul(np.array([d]).T, np.array([d])).shape)

        self._Sigma = \
            (self._epsilon*np.identity( len(self._theta) ) + \
                cov)/(self._numElite + self._epsilon)

        return self._theta
 

    def reset(self)->None:
        # theta_0 = np.zeros(len(self._theta))
        self._theta = self._orig_theta # np.random.multivariate_normal(theta_0, self._sigma*np.identity( len(self._theta) ))
        self._Sigma = self._orig_sigma #*np.identity( len(self._theta) )
        # self._attempts = 0
        # self._ret_collection = np.array([])
