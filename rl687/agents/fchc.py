import numpy as np
from .bbo_agent import BBOAgent

from typing import Callable


class FCHC(BBOAgent):
    """
    First-choice hill-climbing (FCHC) for policy search is a black box optimization (BBO)
    algorithm. This implementation is a variant of Russell et al., 2003. It has 
    remarkably fewer hyperparameters than CEM, which makes it easier to apply. 
    
    Parameters
    ----------
    sigma (float): exploration parameter 
    theta (np.ndarray): initial mean policy parameter vector
    numEpisodes (int): the number of episodes to sample per policy
    evaluationFunction (function): evaluates a provided policy.
        input: policy (np.ndarray: a parameterized policy), numEpisodes
        output: the estimated return of the policy 
    """

    def __init__(self, theta:np.ndarray, sigma:float, evaluationFunction:Callable, numEpisodes:int=10, basis_k:int=4):
        self._name = "First_Choice_Hill_Climbing"
        self._exploration_param = sigma
        self._original_theta = theta
        self._theta = theta #np.random.multivariate_normal(theta_0, exploration_param*np.identity(len(theta_0))) # Initial mean paramter
        self._numEpisodes = numEpisodes
        self._evaluationFunction = evaluationFunction

        self._attempts = 0
        # self._ret_collection = np.array([])
        self._max_nepisodes = 3000
        self._J_opt = self._evaluationFunction(self._theta, self._numEpisodes, basis_k)  
        self._basis_k = basis_k     


    @property
    def name(self)->str:
        return self._name
    
    @property
    def parameters(self)->np.ndarray:
        return self._theta

    def train(self)->np.ndarray:

        # fout = open("fchc_policy_track_2.txt", 'w+')
        # fout_vis = open("fchc_policy_vis_2.txt", 'w+')
        # while (True):
        # while (self._numEpisodes*self._attempts < self._max_nepisodes):
        theta_k = np.random.multivariate_normal(self._theta, self._exploration_param*np.identity(len(self._theta)) )
        J_k = self._evaluationFunction(theta_k, self._numEpisodes, self._basis_k)
        if (J_k > self._J_opt):
            self._theta = theta_k
            self._J_opt = J_k

        # self._attempts += 1

        ############# DEBUG CODE #############
        # if (self._attempts % 5 == 0):
        #     print("Attempt ", self._attempts, " : ", J_k, "/", self._J_opt)

        # if ((self._attempts + 1) % 20 == 0):
        #     for s in range(num_states):
        #         fout.write(str(s) + " : " + str(fchc_policy.getActionProbabilities(s)) + "\n")
        #     fout.write("=====================================")
        #     vis_policy_gridworld(fchc_policy, fout_vis, attempts)

        # np.append(self._ret_collection, J_opt)

        return self._theta

    def reset(self)->None:
        # self._attempts = 0
        # self._ret_collection = np.array([])
        self._theta = self._original_theta # np.random.multivariate_normal(theta_0, self._exploration_param*np.identity( len(self._theta) ) )
        self._J_opt = self._evaluationFunction(self._theta, self._numEpisodes)